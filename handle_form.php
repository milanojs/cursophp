<!D OCT YPE h t m l PUBLIC "-//W3C// DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Form Feedback</title>
</head>
<style type="text/css"  
title="text/css" media="all">
.error {
font-weight: bold;
color: #C00;
}
</style>
<body>
<?php # Script 2.2 - handle_form.php
if (!empty($_REQUEST['name'])) {
$name = $_REQUEST['name'];
} else {
$name = NULL;
echo '<p class="error">You forgot to enter your name!</p>';
}
if (!empty($_REQUEST['email'])) {
$email = $_REQUEST['email'];
} else {
$email = NULL;
echo '<p class="error">You  
forgot to enter your email  
address!</p>';
}
if (!empty($_REQUEST['comments'])) {
$comments = $_REQUEST['comments'];
} else {
$comments = NULL;
echo '<p class="error">You forgot to enter your comments!</p>';
}

$comments = $_REQUEST['comments'];

echo "<p>Thank you, <b>$name</b>,  
for the following comments:<br />
<tt>$comments</tt></p><p>We will reply to you at  
<i>$email</i>.</p>\n";
//Create the $gender variable:
if (isset($_REQUEST['gender'])) {
	$gender = $_REQUEST['gender'];
	if ($gender == 'M') {
	$greeting = '<p><b>Good day, Sir!</b></p>';
	} 
	elseif ($gender == 'F') {
	$greeting = '<p><b>Good day, Madam!</b></p>';
	} 
	else {
	$gender = NULL;
	echo '<p class="error">Gender should be either "M" or "F"!</p>';
	}
	}
else {
$gender = NULL;
echo '<p class="error">You forgot to select your gender!</p>';
}

if ( !empty($_POST['name']) && !empty($_POST['comments']) && !empty($_POST['email']) ) {
echo "<p>Thank you, <b>{$_POST['name']}</b>, for the following comments:<br />
<tt>{$_POST['comments']}</tt></p>p>We will reply to you at <i>{$_POST['email']}</i>.</p>\n";

echo $greeting;
}
else {
echo '<p class="error">Please go back and fill out the form again.</p>';
}
?>

<hr>
</body>
</html>